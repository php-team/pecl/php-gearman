php-gearman (2.1.4-1) unstable; urgency=medium

  * New upstream version 2.1.4

 -- Ondřej Surý <ondrej@debian.org>  Sun, 09 Feb 2025 19:52:05 +0100

php-gearman (2.1.3-1) unstable; urgency=medium

  * New upstream version 2.1.3

 -- Ondřej Surý <ondrej@debian.org>  Sun, 09 Feb 2025 12:54:00 +0100

php-gearman (2.1.2-4) unstable; urgency=medium

  * Upload to unstable

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 19:46:58 +0200

php-gearman (2.1.2-4~exp4) experimental; urgency=medium

  * Rebuild with PHP 8.4.0RC1

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 16:07:04 +0200

php-gearman (2.1.2-4~exp3) experimental; urgency=medium

  * Fix Build-Depend on php-all-dev (>= 2:95~)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 24 Sep 2024 14:49:35 +0200

php-gearman (2.1.2-4~exp2) experimental; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Sep 2024 18:49:38 +0200

php-gearman (2.1.2-4~exp1) experimental; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Sep 2024 18:49:35 +0200

php-gearman (2.1.2-4~) unstable; urgency=medium

  * Bump Build-Depends to dh-php >= 5.5~

 -- Ondřej Surý <ondrej@debian.org>  Sun, 22 Sep 2024 08:12:43 +0200

php-gearman (2.1.2-3) unstable; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Sat, 21 Sep 2024 12:57:23 +0200

php-gearman (2.1.2-2) unstable; urgency=medium

  * Bump default version to PHP 8.4

 -- Ondřej Surý <ondrej@debian.org>  Sat, 06 Jul 2024 17:33:47 +0200

php-gearman (2.1.2-1) unstable; urgency=medium

  * New upstream version 2.1.2

 -- Ondřej Surý <ondrej@debian.org>  Sun, 23 Jun 2024 20:02:53 +0200

php-gearman (2.1.0++-1) unstable; urgency=medium

  * New upstream version 2.1.0
  * Finish the source package split for PHP 7.x and PHP 8.x

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Jun 2023 12:13:04 +0200

php-gearman (2.1.0+1.1.2-12) unstable; urgency=medium

  * Regenerate d/control for PHP 8.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 09 Dec 2022 13:32:39 +0100

php-gearman (2.1.0+1.1.2-11) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:23 +0100

php-gearman (2.1.0+1.1.2-10) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:09:45 +0100

php-gearman (2.1.0+1.1.2-8) unstable; urgency=medium

  * Apply 'remove ZVAL_NEW_ARR usage' to fix PHP 8.1 usage (GH #1691)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 21 Dec 2021 10:53:47 +0100

php-gearman (2.1.0+1.1.2-7) unstable; urgency=medium

  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Tue, 21 Dec 2021 10:52:31 +0100

php-gearman (2.1.0+1.1.2-6) unstable; urgency=medium

  * Remove max version from package.xml
  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:00:53 +0100

php-gearman (2.1.0+1.1.2-5) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:06:46 +0100

php-gearman (2.1.0+1.1.2-4) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:27 +0100

php-gearman (2.1.0+1.1.2-3) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:50:33 +0100

php-gearman (2.1.0+1.1.2-2) unstable; urgency=medium

  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:10:43 +0100

php-gearman (2.1.0+1.1.2-1) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * New upstream version 2.1.0+1.1.2
   + PHP 8.0.x is now supported

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 10:53:24 +0100

php-gearman (2.0.6+1.1.2-10) unstable; urgency=medium

  * Update d/watch to use https://pecl.php.net
  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:39:46 +0100

php-gearman (2.0.6+1.1.2-9) unstable; urgency=medium

  * Finish conversion to debhelper compat level 10
  * Update for dh-php >= 2.0 support
  * Build the extension only for PHP 7

 -- Ondřej Surý <ondrej@debian.org>  Sat, 17 Oct 2020 06:02:57 +0200

php-gearman (2.0.6+1.1.2-8) unstable; urgency=medium

  * Add patch to fix gearman segfault in PHP 7.4

 -- Ondřej Surý <ondrej@debian.org>  Wed, 06 May 2020 15:52:14 +0200

php-gearman (2.0.6+1.1.2-7) unstable; urgency=medium

  * Try PHP 7.4 rebuild

 -- Ondřej Surý <ondrej@debian.org>  Mon, 20 Jan 2020 09:19:19 +0100

php-gearman (2.0.6+1.1.2-6) unstable; urgency=medium

  * Update the PHP version limit patch

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Dec 2019 16:07:39 +0100

php-gearman (2.0.6+1.1.2-5) unstable; urgency=medium

  * Disable the tests properly with dh-php >= 0.35

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 13:15:27 +0100

php-gearman (2.0.6+1.1.2-4) unstable; urgency=medium

  * Really disable the tests

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 13:02:39 +0100

php-gearman (2.0.6+1.1.2-3) unstable; urgency=medium

  * Disable running tests as they are broken

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 12:55:00 +0100

php-gearman (2.0.6+1.1.2-2) unstable; urgency=medium

  * Limit gearman to <= PHP 7.3

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 12:33:56 +0100

php-gearman (2.0.6+1.1.2-1) unstable; urgency=medium

  * New upstream version 2.0.6+1.1.2

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 11:37:11 +0100

php-gearman (2.0.5+1.1.2-4) unstable; urgency=medium

  * No change rebuild for Debian buster

 -- Ondřej Surý <ondrej@debian.org>  Wed, 28 Aug 2019 09:17:51 +0200

php-gearman (2.0.5+1.1.2-3) unstable; urgency=medium

  * Bump the dependency on dh-php to >= 0.33

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Oct 2018 09:43:36 +0000

php-gearman (2.0.5+1.1.2-2) unstable; urgency=medium

  * Add package.xml patch for php-gearman 2.0.x PHP 7 support

 -- Ondřej Surý <ondrej@debian.org>  Mon, 20 Aug 2018 07:05:14 +0000

php-gearman (2.0.5+1.1.2-1) unstable; urgency=medium

  * Update Vcs-* to salsa.d.o
  * Update maintainer email to team+php-pecl@tracker.debian.org
    (Closes: #899779)
  * New upstream version 2.0.5+1.1.2
  * Remove all patches; merged upstream

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Aug 2018 12:15:23 +0000

php-gearman (2.0.3+1.1.2+-3) unstable; urgency=medium

  * Update gbp.conf section name(s)
  * Update Vcs-* links to gitlab.com

 -- Ondřej Surý <ondrej@debian.org>  Fri, 08 Sep 2017 08:51:08 +0200

php-gearman (2.0.3+1.1.2+-2) unstable; urgency=medium

  * Add upstream patch to fix segfault in GearmanWorker::addFunction()

 -- Ondřej Surý <ondrej@debian.org>  Fri, 08 Sep 2017 08:47:48 +0200

php-gearman (2.0.3+1.1.2+-1) unstable; urgency=medium

  * New upstream version 2.0.3+1.1.2+

 -- Ondřej Surý <ondrej@debian.org>  Thu, 30 Mar 2017 11:42:48 +0200

php-gearman (2.0.3+1.1.2-1) unstable; urgency=medium

  * New upstream version 2.0.3+1.1.2

 -- Ondřej Surý <ondrej@debian.org>  Wed, 29 Mar 2017 23:28:31 +0200

php-gearman (2.0.2+1.1.2-1) unstable; urgency=medium

  * Imported Upstream version 2.0.2+1.1.2
  * Remove obsolete patches

 -- Ondřej Surý <ondrej@debian.org>  Mon, 05 Dec 2016 12:07:56 +0100

php-gearman (1.1.2-96-ge77f981+1.1.2+-4) unstable; urgency=medium

  * Fix PHP versions in package-7.xml (Closes: #844383)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 15 Nov 2016 05:25:52 +0100

php-gearman (1.1.2-96-ge77f981+1.1.2+-3) unstable; urgency=medium

  * Properly build-depend on required libgearman-dev (>= 1.0.0)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 21 Jul 2016 16:35:42 +0200

php-gearman (1.1.2-96-ge77f981+1.1.2+-2) unstable; urgency=medium

  * Move the default makefile snippet to dh-php and use a simple d/rules
    with dh-php >= 0.12~

 -- Ondřej Surý <ondrej@debian.org>  Fri, 29 Apr 2016 11:16:45 +0200

php-gearman (1.1.2-96-ge77f981+1.1.2+-1) unstable; urgency=medium

  * Improve d/rules so it can be used with both single or multiple PECL
    upstream versions, controlled by looking for package-MAJOR.MINOR.xml,
    package-MAJOR.xml and package.xml in this order
  * Imported Upstream version 1.1.2-96-ge77f981+1.1.2+
  * Update PHP 7.0 /package/version/release in package-7.xml

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Apr 2016 13:26:03 +0200

php-gearman (1.1.2-96-ge77f981+1.1.2-1) unstable; urgency=medium

  * Use standard master/pristine-tar/upstream branches
  * Imported Upstream version 1.1.2-96-ge77f981+1.1.2
  * Update packaging to co-installable PHP versions
  * Add lintian-override for PHP License

 -- Ondřej Surý <ondrej@debian.org>  Fri, 15 Apr 2016 15:41:20 +0200

php-gearman (1.1.2-1) unstable; urgency=low

  * Initial Release (Closes: #573894).

 -- Ondřej Surý <ondrej@debian.org>  Mon, 21 Oct 2013 11:06:11 +0200
